package registry

import "sync"

// BidirectionalIndex describe bidirectional index
type BidirectionalIndex struct {
	mu           sync.RWMutex
	index        map[interface{}]interface{}
	reverseIndex map[interface{}]interface{}
}

// NewBidirectionalIndex return new bidirectional index
func NewBidirectionalIndex() *BidirectionalIndex {
	return &BidirectionalIndex{
		index:        make(map[interface{}]interface{}),
		reverseIndex: make(map[interface{}]interface{}),
	}
}

// Add add value for reverse search
func (b *BidirectionalIndex) Add(key, value interface{}) {
	b.mu.Lock()
	b.index[key] = value
	b.reverseIndex[value] = key
	b.mu.Unlock()
}

// Get return value
func (b *BidirectionalIndex) Get(key interface{}) (interface{}, bool) {
	b.mu.RLock()
	if v, exists := b.index[key]; exists {
		b.mu.RUnlock()
		return v, true
	}
	if v, exists := b.reverseIndex[key]; exists {
		b.mu.RUnlock()
		return v, true
	}
	b.mu.RUnlock()
	return nil, false
}

// Delete return bidirectional index
func (b *BidirectionalIndex) Delete(key interface{}) {
	b.mu.Lock()
	if v, exists := b.index[key]; exists {
		delete(b.index, key)
		delete(b.reverseIndex, v)
	} else if v, exists := b.reverseIndex[key]; exists {
		delete(b.reverseIndex, key)
		delete(b.index, v)
	}
	b.mu.Unlock()
}

// GetUint32 return uint32 value
func (b *BidirectionalIndex) GetUint32(key interface{}) (uint32, bool) {
	v, exists := b.Get(key)
	if !exists {
		return 0, false
	}
	u, ok := v.(uint32)
	if !ok {
		return 0, false
	}
	return u, true
}
