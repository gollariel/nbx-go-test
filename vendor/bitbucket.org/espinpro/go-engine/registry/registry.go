package registry

import (
	"sync"
	"sync/atomic"
)

// Global keys
const (
	KeyTemporary = "temporary"
)

// Default return global registry instance
func Default() *Registry {
	return Get(nil)
}

// Reset re-create storage
func Reset() {
	ResetByKey(nil)
}

// Registry base structure for store everything. For normalize structure and decrease locks all data stored by groups
type Registry struct {
	aid     uint32
	groups  map[interface{}]*Group
	mu      sync.RWMutex
	indexes map[uint32]interface{}
}

// NewRegistry return new registry
func NewRegistry() *Registry {
	return &Registry{
		groups:  make(map[interface{}]*Group),
		indexes: make(map[uint32]interface{}),
	}
}

// NextID return next id
func (r *Registry) NextID() uint32 {
	return atomic.AddUint32(&r.aid, 1)
}

// GetGroup make new or return existing Group
func (r *Registry) GetGroup(key interface{}) (group *Group) {
	var exists bool
	if dk, ok := key.(DataKey); ok {
		key = dk.Key()
	}

	r.mu.RLock()
	group, exists = r.groups[key]
	r.mu.RUnlock()

	if !exists {
		group = r.initGroup(key)
	}
	return
}

func (r *Registry) initGroup(key interface{}) (group *Group) {
	var exists bool
	r.mu.Lock()
	group, exists = r.groups[key]
	if !exists {
		group = NewGroup()
		r.groups[key] = group
	}
	r.mu.Unlock()
	return
}

// DeleteGroup delete Group
func (r *Registry) DeleteGroup(key interface{}) {
	r.mu.Lock()
	delete(r.groups, key)
	r.mu.Unlock()
}

// AddIndex add index for uint32 id
func (r *Registry) AddIndex(id uint32, key interface{}) {
	r.mu.Lock()
	r.indexes[id] = key
	r.mu.Unlock()
}

// GetIndex get index for uint32 id
func (r *Registry) GetIndex(id uint32) interface{} {
	r.mu.RLock()
	i := r.indexes[id]
	r.mu.RUnlock()
	return i
}

// GetIndexUint32 return uint32 index for uint32 id
func (r *Registry) GetIndexUint32(id uint32) (i uint32, ok bool) {
	r.mu.RLock()
	i, ok = r.indexes[id].(uint32)
	r.mu.RUnlock()
	return
}

// RemIndex remove index for uint32 id
func (r *Registry) RemIndex(id uint32) {
	r.mu.Lock()
	delete(r.indexes, id)
	r.mu.Unlock()
}

// Set set entity
func (r *Registry) Set(key interface{}, id interface{}, e interface{}) error {
	group := r.GetGroup(key)
	return group.Set(id, e)
}

// Get get entity
func (r *Registry) Get(key interface{}, id interface{}) (e interface{}, err error) {
	group := r.GetGroup(key)
	return group.Get(id)
}

// Delete delete entity
func (r *Registry) Delete(key interface{}, id interface{}) error {
	group := r.GetGroup(key)
	return group.Delete(id)
}

// GetAllInGroup return all entities for Group
func (r *Registry) GetAllInGroup(key interface{}) []interface{} {
	group := r.GetGroup(key)
	return group.GetAll()
}

// TickGroup call tick for each node in Group
func (r *Registry) TickGroup(key interface{}) {
	group := r.GetGroup(key)
	group.Tick(key)
}

// TickInOrder tick in order
func (r *Registry) TickInOrder(keys []interface{}) {
	for _, key := range keys {
		r.TickGroup(key)
	}
}

// AsyncTickByKeys async tick in order
func (r *Registry) AsyncTickByKeys(keys []interface{}) {
	var wg sync.WaitGroup
	wg.Add(len(keys))
	for _, key := range keys {
		go func(key interface{}) {
			r.TickGroup(key)
			wg.Done()
		}(key)
	}
	wg.Wait()
}

// ClearGroup clear all entities in Group
func (r *Registry) ClearGroup(key interface{}) {
	group := r.GetGroup(key)
	group.Clear()
}

// SearchInGroup search by registry
func (r *Registry) SearchInGroup(key interface{}, f SearchFunction) chan interface{} {
	result := make(chan interface{}, 1000)
	group := r.GetGroup(key)
	go func(key interface{}, result chan interface{}, f SearchFunction) {
		group.Search(key, result, f)
		close(result)
	}(key, result, f)
	return result
}

// SearchOne search single document
func (r *Registry) SearchOne(key interface{}, f SearchFunction) interface{} {
	group := r.GetGroup(key)
	return group.SearchOne(key, f)
}

// SafeCall safe call in group
func (r *Registry) SafeCall(key interface{}, f func(map[interface{}]interface{}, *RTree, *SortedSet, *BidirectionalIndex)) {
	group := r.GetGroup(key)
	group.SafeCall(f)
}
