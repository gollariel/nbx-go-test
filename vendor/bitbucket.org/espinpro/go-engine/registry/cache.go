package registry

import (
	"time"

	"github.com/patrickmn/go-cache"
)

// Cache configuration
const (
	CacheExpiration = 2 * time.Hour
	CleanupInterval = time.Hour
)

var c = cache.New(CacheExpiration, CleanupInterval)

// GetCache return cache
func GetCache() *cache.Cache {
	return c
}
