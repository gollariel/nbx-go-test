package registry

import "bitbucket.org/espinpro/go-engine/geometry/shapes"

// Count of workers
const workersCount = 4

// Channel buffer size
const bufferSize = 1000

// Destroyable able to be destroyed in registry
type Destroyable interface {
	Destroy() error
}

// Constructable able to be destroyed in registry
type Constructable interface {
	Construct() error
}

// SearchFunction describe specification for filter function
type SearchFunction func(key interface{}, id interface{}, data interface{}) bool

// Ticker describe function for execute tick
type Ticker interface {
	Tick()
}

// DataKey describe key type
type DataKey interface {
	Key() interface{}
}

// Moveable objects what ability to update themself
type Moveable interface {
	shapes.Spatial
	UpdateSpatial(s shapes.Spatial)
}
