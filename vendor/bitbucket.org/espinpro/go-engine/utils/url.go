package utils

import (
	"errors"
	"net/url"
)

// ErrWrongCountOfArguments fired when count of arguments is not expected
var ErrWrongCountOfArguments = errors.New("error wrong count of arguments")

// AppendQueryToURL append query to given url
func AppendQueryToURL(u string, keyValues ...string) (string, error) {
	l := len(keyValues)
	if l%2 != 0 || l == 0 {
		return "", ErrWrongCountOfArguments
	}
	parsedURL, err := url.Parse(u)
	if err != nil {
		return u, err
	}
	q := parsedURL.Query()
	for i := 0; i < len(keyValues)-1; i += 2 {
		key, value := keyValues[i], keyValues[i+1]
		q.Set(key, value)
	}
	parsedURL.RawQuery = q.Encode()
	return parsedURL.String(), nil
}
