package utils

import "math"

// CantorPair make one value from two values
func CantorPair(x, y float64) float64 {
	return ((x+y)*(x+y+1))/2 + y
}

// ReverseCantorPair return two values from one
func ReverseCantorPair(z float64) (float64, float64) {
	w := math.Floor((math.Sqrt(float64(8*z+1)) - 1) / 2)
	t := (w*w + w) / 2
	y := z - t
	x := w - y
	return x, y
}

// CantorPairUint32 make one value from two values
func CantorPairUint32(x, y uint32) uint32 {
	return uint32(CantorPair(float64(x), float64(y)))
}

// ReverseCantorPairUint32 return two values from one
func ReverseCantorPairUint32(z uint32) (uint32, uint32) {
	x, y := ReverseCantorPair(float64(z))
	return uint32(x), uint32(y)
}
