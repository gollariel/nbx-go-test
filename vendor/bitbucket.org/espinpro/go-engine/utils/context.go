package utils

// ContextKey contains context key
type ContextKey string

// NewContextKey return ContextKey
func NewContextKey(value string) ContextKey {
	return ContextKey(value)
}
