package utils

import (
	"os"

	"github.com/google/gops/agent"
)

// RunGopsInstance run gops instance if GOPS_ADDR present
func RunGopsInstance() error {
	gopsAddr := os.Getenv("GOPS_ADDR")
	if gopsAddr == "" {
		gopsAddr = ":6060"
	}
	if err := agent.Listen(agent.Options{Addr: gopsAddr}); err != nil {
		return err
	}
	return nil
}
