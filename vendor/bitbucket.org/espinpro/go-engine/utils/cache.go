package utils

import (
	"time"

	"github.com/patrickmn/go-cache"
)

// GetCache return cache
func GetCache(expiration time.Duration, cleanupInterval time.Duration) *cache.Cache {
	return cache.New(expiration, cleanupInterval)
}
