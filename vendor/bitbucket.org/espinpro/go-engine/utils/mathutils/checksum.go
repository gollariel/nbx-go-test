package mathutils

import (
	"crypto/md5"
	"encoding/hex"
	"hash/crc32"
)

// Crc32Mod gets crc32 checksum by given str and return module of the checksum
func Crc32Mod(str string, modulus uint32) uint32 {
	if modulus == 0 {
		return 0
	}
	return crc32.ChecksumIEEE([]byte(str)) % modulus
}

// Md5Crc32Mod gets md5 hashed crc32 checksum by given str and return module of the checksum
func Md5Crc32Mod(str string, modulus uint32) uint32 {
	if modulus == 0 {
		return 0
	}
	md5Str := md5.Sum([]byte(str))
	return crc32.ChecksumIEEE([]byte(hex.EncodeToString(md5Str[:]))) % modulus
}
