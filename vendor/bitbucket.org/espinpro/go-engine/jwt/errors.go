package jwt

import "bitbucket.org/espinpro/go-engine/errors"

// All kind of errors for jwt
const (
	ErrUnexpectedSigningMethod = errors.ErrorString("unexpected signing method")
	ErrUserNotInScope          = errors.ErrorString("not in scope")
	ErrUserWrongService        = errors.ErrorString("wrong service")
	ErrInvalidToken            = errors.ErrorString("invalid token")
)
