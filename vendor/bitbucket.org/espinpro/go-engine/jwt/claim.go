package jwt

import (
	"bitbucket.org/espinpro/go-engine/datatypes"
	"github.com/dgrijalva/jwt-go"
)

// Claim custom claim
type Claim struct {
	jwt.StandardClaims
	Scopes datatypes.DataSetStrings `json:"scp,omitempty"`
}
