package communications

import "context"

// Command interface for standard command
type Command interface {
	GetMsgType() uint8
	Decode(pid uint32, msg []byte)
	GetPlayerID() uint32
	Execute(ctx context.Context) error
}

// CommandParser command parser
type CommandParser interface {
	Parse(pid uint32, msg []byte) (cmd Command, err error)
}
