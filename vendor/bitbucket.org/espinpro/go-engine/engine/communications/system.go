package communications

import (
	"context"
	"sync"

	"bitbucket.org/espinpro/go-engine/registry"
)

const workersCount = 5
const channelBuffer = 1000

// OutgoingMessage describe outgoing message
type OutgoingMessage interface {
	GetMessageType() uint8
	Encode() []byte
}

// Communication describe communication
type Communication interface {
	ProcessOutgoingQueue()
	AddMessageToQueue(m OutgoingMessage)
	GetIncoming() chan []byte
	GetOutgoing() chan []byte
	ProcessIncomingMessages(ctx context.Context, id uint32, e []byte) error
	Close() error
	Write(d []byte)
}

// CommunicationSystem contains moveable entities
type CommunicationSystem struct {
	keys []interface{}
}

// NewCommunicationSystem return new CommunicationSystem
func NewCommunicationSystem(keys ...interface{}) *CommunicationSystem {
	return &CommunicationSystem{
		keys: keys,
	}
}

// EntitiesKeys return entities keys
func (c *CommunicationSystem) EntitiesKeys() []interface{} {
	return c.keys
}

// Update update move
func (c *CommunicationSystem) Update(ctx context.Context) error {
	for _, key := range c.EntitiesKeys() {
		g := registry.Default().GetGroup(key).GetGenerator()

		communication := make(chan interface{}, channelBuffer)

		var wg sync.WaitGroup
		wg.Add(workersCount)
		for i := 0; i < workersCount; i++ {
			go func() {
				for e := range communication {
					m, ok := e.(Communication)
					if !ok {
						continue
					}

					m.ProcessOutgoingQueue()
				}
				wg.Done()
			}()
		}

		for e := range g {
			communication <- e
		}
		close(communication)
		wg.Wait()
	}
	return nil
}
