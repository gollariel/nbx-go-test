package server

import (
	"net/http"
)

// ListenAndServe start listen for connections
func ListenAndServe(handler http.Handler, cfg *ConnectionConfig) error {
	server := &http.Server{
		Addr:         cfg.Addr,
		Handler:      handler,
		ReadTimeout:  cfg.ReadTimeout,
		WriteTimeout: cfg.WriteTimeout,
	}
	return server.ListenAndServe()
}
