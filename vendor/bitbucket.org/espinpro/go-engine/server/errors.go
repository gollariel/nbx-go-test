package server

import "errors"

// Server errors
var (
	ErrPanicInRead  = errors.New("panic in read")
	ErrPanicInWrite = errors.New("panic in write")
)
