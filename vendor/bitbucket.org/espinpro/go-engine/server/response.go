package server

import (
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/espinpro/go-engine/fastlog"
)

const (
	contentTypeJSON = "application/json; charset=utf-8"
)

// ResponseJSON default response
type ResponseJSON struct {
	Data       map[string]interface{} `json:"data"`
	ServerInfo map[string]interface{} `json:"serverInfo"`
	status     int
	header     map[string]string
}

// NewResponseJSON return new json response
func NewResponseJSON() *ResponseJSON {
	return &ResponseJSON{
		Data: map[string]interface{}{},
		ServerInfo: map[string]interface{}{
			"timestamp": time.Now().Unix(),
		},
		status: http.StatusOK,
		header: map[string]string{},
	}
}

// Set set Status and prepared Data
func (r *ResponseJSON) Set(data map[string]interface{}, status int) *ResponseJSON {
	r.Data = data
	r.status = status
	return r
}

// Add add data into response
func (r *ResponseJSON) Add(key string, value interface{}) *ResponseJSON {
	r.Data[key] = value
	return r
}

// Header add header
func (r *ResponseJSON) Header(key string, value string) *ResponseJSON {
	r.header[key] = value
	return r
}

// SetStatus set status
func (r *ResponseJSON) SetStatus(status int) *ResponseJSON {
	r.status = status
	return r
}

// Debug add Data to serverInfor
func (r *ResponseJSON) Debug(debug map[string]interface{}) *ResponseJSON {
	for k, v := range debug {
		r.ServerInfo[k] = v
	}
	return r
}

// Send send json response
func (r *ResponseJSON) Send(w http.ResponseWriter) {
	data, err := json.Marshal(r)
	if err != nil {
		fastlog.Error("Error marshal data", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err = w.Write([]byte(err.Error()))
		if err != nil {
			fastlog.Error("Error sending response", "err", err)
		}
	}

	w.Header().Add("Content-Type", contentTypeJSON)
	for k, v := range r.header {
		w.Header().Add(k, v)
	}
	w.WriteHeader(r.status)
	_, err = w.Write(data)
	if err != nil {
		fastlog.Error("Error sending response", "err", err)
	}
}

// Send send response
func Send(w http.ResponseWriter, contentType string, status int, data []byte, header map[string]string) {
	w.WriteHeader(status)
	w.Header().Add("Content-Type", contentType)
	if header != nil {
		for k, v := range header {
			w.Header().Add(k, v)
		}
	}
	if contentType == "" {
		contentType = contentTypeJSON
	}
	_, err := w.Write(data)
	if err != nil {
		fastlog.Error("Error sending response", "err", err)
	}
}
