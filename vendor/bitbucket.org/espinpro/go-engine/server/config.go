package server

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

// ConnectionConfig contains listen url for the server and additional options
type ConnectionConfig struct {
	Addr         string        `required:"true"`
	ReadTimeout  time.Duration `default:"60s" split_words:"true"`
	WriteTimeout time.Duration `default:"60s" split_words:"true"`
}

// NewConnectionConfig return new server connection config
func NewConnectionConfig(addr string, readTimeout, writeTimeout time.Duration) *ConnectionConfig {
	return &ConnectionConfig{
		Addr:         addr,
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
	}
}

// GetConnectionConfigFromEnv return server configs bases on environment variables
func GetConnectionConfigFromEnv(prefix string) (*ConnectionConfig, error) {
	c := new(ConnectionConfig)
	err := envconfig.Process(prefix, c)
	return c, err
}
