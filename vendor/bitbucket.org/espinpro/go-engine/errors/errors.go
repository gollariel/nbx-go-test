package errors

// ErrorString contains error string
type ErrorString string

// Error return error message
func (e ErrorString) Error() string {
	return string(e)
}
