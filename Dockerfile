FROM golang:1.15-buster as build
WORKDIR /go/src/bitbucket.org/gollariel/nbx-go-test
COPY . .
ENV GOFLAGS=-mod=vendor
RUN make build_userservice
RUN cp ./bin/userservice /binary

FROM ubuntu:latest
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /binary .
RUN chmod +x /binary
ENTRYPOINT ["/binary"]