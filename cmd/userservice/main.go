package main

import (
	"net/http"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/server"
	"bitbucket.org/espinpro/go-engine/server/middlewares"
)

var version string

func main() {
	c, err := server.GetConnectionConfigFromEnv("US")
	if err != nil {
		fastlog.Fatal("Error getting config", "err", err)
	}
	fastlog.Important("Starting server", "version", version, "addr", c.Addr)

	m := server.NewMiddleware(middlewares.Recover)
	i := server.Root()
	i.Add("/",
		server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			SendJSON(w, http.StatusOK, map[string]string{"name": "user-service"}, nil)
		})))
	i.Add("/users",
		server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			SendJSON(w, http.StatusOK, []string{}, nil)
		})),
		server.NewRoute(http.MethodPost, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			SendJSON(w, http.StatusCreated, map[string]string{}, nil)
		})))
	i.Add("/users/:user_id",
		server.NewRoute(http.MethodPut, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			SendJSON(w, http.StatusOK, map[string]string{}, nil)
		})),
		server.NewRoute(http.MethodDelete, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			SendJSON(w, http.StatusNoContent, nil, nil)
		})),
	)
	if err := server.ListenAndServe(m.Then(i.Handler(nil)), c); err != nil {
		fastlog.Fatal("Error executing server", "err", err)
	}
}
