package main

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/espinpro/go-engine/fastlog"
)

const (
	contentTypeJSON = "application/json; charset=utf-8"
)

// SendJSON send JSON response
func SendJSON(w http.ResponseWriter, status int, data interface{}, header map[string]string) {
	var d []byte
	var err error
	if data != nil {
		d, err = json.Marshal(data)
		if err != nil {
			fastlog.Error("Error marshal data", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			_, err = w.Write([]byte(err.Error()))
			if err != nil {
				fastlog.Error("Error sending response", "err", err)
			}
		}
	}

	w.WriteHeader(status)
	w.Header().Add("Content-Type", contentTypeJSON)
	if header != nil {
		for k, v := range header {
			w.Header().Add(k, v)
		}
	}
	_, err = w.Write(d)
	if err != nil {
		fastlog.Error("Error sending response", "err", err)
	}
}
